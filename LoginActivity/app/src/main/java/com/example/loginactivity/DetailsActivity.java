package com.example.loginactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Button ok = findViewById(R.id.ok);
        ok.setOnClickListener(this);

        TextView text2 = findViewById(R.id.userNameAf2);


        // récupérer le contexte d'application et la donnée qu'elle contient
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        String login = app.getLogin();
        text2.setText(login);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);


    }
}
