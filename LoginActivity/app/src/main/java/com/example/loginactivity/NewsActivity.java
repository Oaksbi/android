package com.example.loginactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {

    // A la creation de l'activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        /* Bouton pour aller sur l'activity DetailsActivity */
        Button details = findViewById(R.id.details);
        details.setOnClickListener(this);

        /* Bouton pour revenir vers l'activity MainActivity */
        Button logout = findViewById(R.id.logout);
        logout.setOnClickListener(this);

        /* Bouton pour aller sur le site du prof */
        Button about = findViewById(R.id.about);
        about.setOnClickListener(this);

        /*Zone de texte non modifiable pour afficher le login récupérer de la MainActivity */
        TextView text = findViewById(R.id.userNameAf);

        /*A la creation, Récupérer le login de la MainActivity en utilisant les extras */
        Intent intent = getIntent();
        String login = intent.getStringExtra("login");
        text.setText(login);

    }


    @Override
    public void onClick(View view) {
        Intent intent;

        switch (view.getId()) {

            // clic sur le bouton Details : redirection vers l'activity DetailsActivity
            case R.id.details:
                intent = new Intent(this, DetailsActivity.class);
                startActivity(intent);
                break;

            // clic sur le bouton Logout : redirection vers l'activity MainActivity
            case R.id.logout:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;

            // clic sur le bouton About : redirection vers le site du prof
            case R.id.about:
                String url = "http://android.busin.fr/";
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break;

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    private static final String TAG = "NewsList";
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "terminaison de l'activité "+getLocalClassName());
    }

}
