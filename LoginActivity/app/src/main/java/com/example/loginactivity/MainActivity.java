package com.example.loginactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button login = findViewById(R.id.login);
        login.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {

        EditText userName = findViewById(R.id.userName);
        String chaine= userName.getText().toString();
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        app.setLogin(chaine);

        Intent intent = new Intent(this, NewsActivity.class);
        intent.putExtra("login",chaine);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    private static final String TAG = "NewsList";

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "terminaison de l'activité "+getLocalClassName());
    }

}
