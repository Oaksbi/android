package com.example.td4;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {


    RelativeLayout parentlayout;

    private TextView name;



    // itemView : vue pour chaque cellule
    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        //mImageTV = itemView.findViewById(R.id.Image);
        name = itemView.findViewById(R.id.Name);
        //parentlayout = itemView.findViewById(R.id.parentLayout);

    }


    void display(item jeuVideo){
        name.setText(jeuVideo.getName());

    }
}
