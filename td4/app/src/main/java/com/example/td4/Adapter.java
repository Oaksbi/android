package com.example.td4;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<ViewHolder> {


    private List<item> maliste;


    public Adapter(List<item> maliste) {
        this.maliste = maliste;
    }

    @NonNull
    @Override
    // Executee lors de la creation du viewholder
    // Execitee une seule fois
    // On charge la vue .xml  de notre cellule
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.display(maliste.get(position));
    }

    @Override
    public int getItemCount() {
        return maliste.size();
    }


}
