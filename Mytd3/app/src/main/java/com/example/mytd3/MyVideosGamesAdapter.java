package com.example.mytd3;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyVideosGamesAdapter extends RecyclerView.Adapter<MyVideoGamesViewHolder> {


    private List<JeuVideo> mesJeux;

    //private List<JeuVideo> mesImages;

    public MyVideosGamesAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
        //this.mesImages = mesImages;
    }

    @NonNull
    @Override
    // Executee lors de la creation du viewholder
    // Execitee une seule fois
    // On charge la vue .xml  de notre cellule
    public MyVideoGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,
                parent, false);
        return new MyVideoGamesViewHolder(view);
    }


    @Override
    // Executee a la liaison avec l'adapter ie Recyclage dune cellule
    // instance du viewholder + sa position
    // Mise a jour de la cellule courante, rafraichir les textview
    public void onBindViewHolder(@NonNull MyVideoGamesViewHolder holder, final int position) {
        holder.display(mesJeux.get(position));

    }

    @Override
    // Renvoyer le nombre d’items de la liste
    public int getItemCount() {
        return mesJeux.size();
    }
}
