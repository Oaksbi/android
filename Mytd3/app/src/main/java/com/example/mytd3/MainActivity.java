package com.example.mytd3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //la  LISTE DE JEUX VIDEO NOMMEE mesJeux ET REMPLIE de JeuxVideo

        List<JeuVideo> mesJeux = new ArrayList<JeuVideo>();

        mesJeux.add(new JeuVideo("FIFA 2020", 100));
        mesJeux.add(new JeuVideo("WWE", 110));
        mesJeux.add(new JeuVideo("IRON MAN", 110));
        mesJeux.add(new JeuVideo("SPIDER MAN", 110));
        mesJeux.add(new JeuVideo("DRAGON BALL Z", 110));
        mesJeux.add(new JeuVideo("SONIC", 110));


        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter( new MyVideosGamesAdapter(mesJeux));

        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
    }
}
