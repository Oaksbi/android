package com.example.mytd3;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyVideoGamesViewHolder extends  RecyclerView.ViewHolder {

    RelativeLayout parentlayout;
   // private CircleImageView mImageTV;
    private TextView mNameTV;
    private TextView mPriceTV;


    // itemView : vue pour chaque cellule
    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);

        //mImageTV = itemView.findViewById(R.id.Image);
        mNameTV = itemView.findViewById(R.id.Name);
        mPriceTV = itemView.findViewById(R.id.Price);
        //parentlayout = itemView.findViewById(R.id.parentLayout);

    }


    void display(JeuVideo jeuVideo){
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "$");
    }


}
