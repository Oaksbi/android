package com.example.td;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class MainActivity extends AppCompatActivity {

    private EditText operations;
    private TextView result;
    private Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9,
                    compute,
                    plus, minus, times, divBy,
                    rBracket, lBracket,
                    clear, neg, comma;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        operations = (EditText) findViewById(R.id.operations);
        result = (TextView) findViewById(R.id.result);
        btn0 = (Button)findViewById(R.id.btn0);
        btn1 = (Button)findViewById(R.id.btn1);
        btn2 = (Button)findViewById(R.id.btn2);
        btn3 = (Button)findViewById(R.id.btn3);
        btn4 = (Button)findViewById(R.id.btn4);
        btn5 = (Button)findViewById(R.id.btn5);
        btn6 = (Button)findViewById(R.id.btn6);
        btn7 = (Button)findViewById(R.id.btn7);
        btn8 = (Button)findViewById(R.id.btn8);
        btn9 = (Button)findViewById(R.id.btn9);
        comma = (Button)findViewById(R.id.comma);
        neg = (Button)findViewById(R.id.neg);
        plus = (Button)findViewById(R.id.plus);
        minus = (Button)findViewById(R.id.minus);
        times = (Button)findViewById(R.id.times);
        divBy = (Button)findViewById(R.id.divBy);
        rBracket = (Button)findViewById(R.id.rBracket);
        lBracket = (Button)findViewById(R.id.lBracket);
        clear = (Button)findViewById(R.id.clear);
        neg = (Button)findViewById(R.id.neg);

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "0");
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "4");
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "9");
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "+");
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "-");
            }
        });

        neg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "-");
            }
        });


        times.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "x");
            }
        });

        divBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "÷");
            }
        });

        comma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + ".");
            }
        });

        lBracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + "(");
            }
        });

        rBracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText(operations.getText() + ")");
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operations.setText("");
            }
        });






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void compute(View v){


        String operation;
        operation = (String) operations.getText().toString();
        operation = operation.replace("x","*");
        operation = operation.replace("÷","/");
        ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");

        try {

            Object resultat = engine.eval(operation);

            result.setText(String.valueOf(resultat));
        }
        catch (ScriptException e) {

            e.printStackTrace();
        }


    }



}
